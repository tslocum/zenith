# zenith
[![GoDoc](https://gitlab.com/tslocum/godoc-static/-/raw/master/badge.svg)](https://docs.rocketnine.space/gitlab.com/tslocum/zenith)
[![CI status](https://gitlab.com/tslocum/zenith/badges/master/pipeline.svg)](https://gitlab.com/tslocum/zenith/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Game match tracking library

## Features

- Glicko2 rating system (WIP)
- Web-based leaderboards (WIP)

## Dependencies

- [jlouis/glicko2](https://github.com/jlouis/glicko2)

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/zenith/issues).
