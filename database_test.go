package zenith

import (
	"fmt"

	"github.com/DATA-DOG/go-sqlmock"
)

func newMockDatabase() (*Database, sqlmock.Sqlmock, error) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		return nil, nil, fmt.Errorf("unexpected error while opening mock database connection: %s", err)
	}

	db := &Database{
		db:           mockDB,
		funcGreatest: "GREATEST",
		driver:       "sqlmock",
	}

	//mock.ExpectExec("UPDATE products").WillReturnResult(sqlmock.NewResult(1, 1))

	/*

		rows := sqlmock.NewRows([]string{"id", "title", "body"}).
			AddRow(1, "post 1", "hello").
			AddRow(2, "post 2", "world")

		mock.ExpectQuery("^SELECT (.+) FROM match $").WillReturnRows(rows)*/

	return db, mock, nil
}
