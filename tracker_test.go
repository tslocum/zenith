package zenith

import (
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
)

var testResults = []*MatchResult{
	// Match 1
	{
		ID:        1,
		Timestamp: 637632000,
		Game:      1,
		Match:     1,
		Player:    1,
		Outcome:   Won,
		MetaI: map[string]int{
			"timestamp": int(time.Now().Local().Unix()),
		},
		MetaS: map[string]string{
			"map": "de_dust2",
		},
	},
	{
		ID:        2,
		Timestamp: 637632000,
		Game:      1,
		Match:     1,
		Player:    2,
		Outcome:   Lost,
		MetaI: map[string]int{
			"timestamp": int(time.Now().Local().Unix()),
		},
		MetaS: map[string]string{
			"map": "de_dust2",
		},
	},

	// Match 2
	{
		ID:        3,
		Timestamp: 637632002,
		Game:      1,
		Match:     2,
		Player:    1,
		Outcome:   Drew,
		MetaI: map[string]int{
			"timestamp": int(time.Now().Local().Unix()),
		},
		MetaS: map[string]string{
			"map": "de_inferno",
		},
	},
	{
		ID:        4,
		Timestamp: 637632002,
		Game:      1,
		Match:     2,
		Player:    2,
		Outcome:   Drew,
		MetaI: map[string]int{
			"timestamp": int(time.Now().Local().Unix()),
		},
		MetaS: map[string]string{
			"map": "de_inferno",
		},
	},
}

func TestTracker(t *testing.T) {
	db, mock, err := newMockDatabase()

	options := &TrackerOptions{
		Database: db,
	}
	tracker, err := NewTracker(options)
	if err != nil {
		t.Errorf("failed to create new tracker: %s", err)
	}

	for i := range testResults {
		mock.ExpectExec("INSERT INTO match").WithArgs(testResults[i].Timestamp, testResults[i].Game, testResults[i].Match, testResults[i].Player, testResults[i].Outcome).WillReturnResult(sqlmock.NewResult(1, 1))

		err := tracker.Track(testResults[i])
		if err != nil {
			t.Errorf("failed to track match result %d: %s", i, err)
		}
	}

	for player := 1; player <= 2; player++ {
		matchRows := sqlmock.NewRows([]string{"id", "timestamp", "game", "match", "player", "outcome"})
		for i := range testResults {
			if testResults[i].Player == player {
				matchRows.AddRow(testResults[i].ID, testResults[i].Timestamp, testResults[i].Game, testResults[i].Match, testResults[i].Player, testResults[i].Outcome)
			}
		}
		mock.ExpectQuery("SELECT").WillReturnRows(matchRows)

		matches, err := db.Matches(1, player)
		if err != nil {
			t.Errorf("failed to fetch player matches: %s", err)
		}

		if len(matches) != 2 {
			t.Fatalf("failed to fetch player matches: expected 2 matches, got %+v", matches)
		}

		for i := range matches {
			if matches[i].Player != player {
				t.Fatalf("failed to fetch player matches: expected player %d, got %d", player, matches[i].Player)
			}
		}

		wins := 1
		losses := 0
		if player == 2 {
			wins = 0
			losses = 1
		}

		mock.ExpectQuery("SELECT").
			WillReturnRows(sqlmock.NewRows([]string{"c"}).AddRow(wins))

		mock.ExpectQuery("SELECT").
			WillReturnRows(sqlmock.NewRows([]string{"c"}).AddRow(1))

		mock.ExpectQuery("SELECT").
			WillReturnRows(sqlmock.NewRows([]string{"c"}).AddRow(losses))

		p, err := db.Player(1, player)
		if err != nil {
			t.Fatalf("failed to fetch player: %s", err)
		}
		if player == 1 {
			if p.Wins != 1 {
				t.Fatalf("failed to fetch player: expected 1 win, got %d", p.Wins)
			} else if p.Draws != 1 {
				t.Fatalf("failed to fetch player: expected 1 draw, got %d", p.Draws)
			} else if p.Losses != 0 {
				t.Fatalf("failed to fetch player: expected 0 losses, got %d", p.Losses)
			}
		} else {
			if p.Wins != 0 {
				t.Fatalf("failed to fetch player: expected 0 wins, got %d", p.Wins)
			} else if p.Draws != 1 {
				t.Fatalf("failed to fetch player: expected 1 draw, got %d", p.Draws)
			} else if p.Losses != 1 {
				t.Fatalf("failed to fetch player: expected 1 loss, got %d", p.Losses)
			}
		}
	}

	// TODO Validate game totals and player totals

	err = mock.ExpectationsWereMet()
	if err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}
