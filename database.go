package zenith

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"sync"
)

// TODO: Add indexes
var DatabaseTables = map[string][]string{
	"match": {
		"`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT",
		"`timestamp` INTEGER NOT NULL DEFAULT 0",
		"`game` INTEGER NOT NULL DEFAULT 0",
		"`match` INTEGER NOT NULL DEFAULT 0",
		"`player` INTEGER NOT NULL DEFAULT 0",
		"`outcome` INTEGER NOT NULL DEFAULT 0",
	},
	"matchmeta": {
		"`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT",
		"`game` INTEGER NOT NULL DEFAULT 0",
		"`match` INTEGER NOT NULL DEFAULT 0",
		"`player` INTEGER NOT NULL DEFAULT 0",
		"`key` VARCHAR(255) NOT NULL DEFAULT ''",
		"`value` TEXT NOT NULL DEFAULT ''",
	},
	"meta": {
		"`key` VARCHAR(255) NOT NULL PRIMARY KEY",
		"`value` TEXT NOT NULL DEFAULT ''",
	}}

const DatabaseVersion = 1

var DatabasePrefix string

type Database struct {
	db           *sql.DB
	funcGreatest string
	driver       string
	sync.RWMutex
}

func Connect(driver string, dataSource string) (*Database, error) {
	var err error
	d := new(Database)

	d.db, err = sql.Open(driver, dataSource)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to database: %s", err)
	}

	d.funcGreatest = "GREATEST"
	if driver == "sqlite3" {
		d.funcGreatest = "MAX"

		_, err = d.db.Exec(`PRAGMA encoding="UTF-8"`)
		if err != nil {
			return nil, fmt.Errorf("failed to send PRAGMA: %s", err)
		}
	}

	err = d.CreateTables()
	if err != nil {
		_ = d.db.Close()
		return nil, fmt.Errorf("failed to create tables: %s", err)
	}

	err = d.Migrate()
	if err != nil {
		_ = d.db.Close()
		return nil, fmt.Errorf("failed to migrate database: %s", err)
	}

	return d, nil
}

func (d *Database) CreateTables() error {
	var (
		tcolumns string
		err      error
	)

	createQueryExtra := ""
	if d.driver == "mysql" {
		createQueryExtra = " ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci"
	}

	for tname, tcols := range DatabaseTables {
		tname = DatabasePrefix + tname

		tcolumns = strings.Join(tcols, ",")
		if d.driver == "mysql" {
			tcolumns = strings.Replace(tcolumns, "AUTOINCREMENT", "AUTO_INCREMENT", -1)
		}

		_, err = d.db.Exec(fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` (%s)", tname, tcolumns) + createQueryExtra)
		if err != nil {
			return fmt.Errorf("failed to create table %s: %s", tname, err)
		}
	}

	return nil
}

func (d *Database) Migrate() error {
	rows, err := d.db.Query("SELECT `value` FROM "+DatabasePrefix+"meta WHERE `key`=?", "version")
	if err != nil {
		return fmt.Errorf("failed to fetch database version: %s", err)
	}

	version := 0
	for rows.Next() {
		v := ""
		err = rows.Scan(&v)
		if err != nil {
			return fmt.Errorf("failed to scan database meta: %s", err)
		}

		version, err = strconv.Atoi(v)
		if err != nil {
			version = -1
		}
	}

	if version == -1 {
		panic("Unable to migrate database: database version unknown")
	} else if version == 0 {
		_, err := d.db.Exec("UPDATE "+DatabasePrefix+"meta SET `value`=? WHERE `key`=?", strconv.Itoa(DatabaseVersion), "version")
		if err != nil {
			return fmt.Errorf("failed to save database version: %s", err)
		}
	}

	migrated := false
	for version < DatabaseVersion {
		switch version {
		case 1:
			// DatabaseVersion 2 migration queries will go here
		}

		version++
		migrated = true
	}

	if migrated {
		_, err := d.db.Exec("UPDATE "+DatabasePrefix+"meta SET `value`=? WHERE `key`=?", strconv.Itoa(DatabaseVersion), "version")
		if err != nil {
			return fmt.Errorf("failed to save updated database version: %s", err)
		}
	}

	return nil
}

func (d *Database) Track(results ...*MatchResult) error {
	d.Lock()
	defer d.Unlock()

	for i := range results {
		_, err := d.db.Exec("INSERT INTO "+DatabasePrefix+"match (`timestamp`, `game`, `match`, `player`, `outcome`) VALUES (?, ?, ?, ?, ?)", results[i].Timestamp, results[i].Game, results[i].Match, results[i].Player, results[i].Outcome)
		if err != nil {
			return fmt.Errorf("failed to track match %+v: %s", results[i], err)
		}
	}

	return nil
}

// TODO Allow -1 player to select all
func (d *Database) Matches(game, player int) ([]*MatchResult, error) {
	d.Lock()
	defer d.Unlock()

	rows, err := d.db.Query("SELECT * FROM "+DatabasePrefix+"match WHERE `game`=? AND `player`=?", game, player)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch matches: %s", err)
	}
	defer rows.Close()

	var matches []*MatchResult
	for rows.Next() {
		match, err := d.scanMatch(rows)
		if err != nil {
			return nil, fmt.Errorf("failed to scan matches: %s", err)
		}

		matches = append(matches, match)
	}

	return matches, nil
}

func (d *Database) Player(game, player int) (*Player, error) {
	d.Lock()
	defer d.Unlock()

	p := &Player{}

	rows, err := d.db.Query("SELECT COUNT(*) as c FROM "+DatabasePrefix+"match WHERE `game`=? AND `player`=? AND `outcome`=?", game, player, Won)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch player: %s", err)
	}
	for rows.Next() {
		err := rows.Scan(&p.Wins)
		if err != nil {
			rows.Close()
			return nil, fmt.Errorf("failed to fetch player: %s", err)
		}
	}
	rows.Close()

	rows, err = d.db.Query("SELECT COUNT(*) as c FROM "+DatabasePrefix+"match WHERE `game`=? AND `player`=? AND `outcome`=?", game, player, Drew)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch player: %s", err)
	}
	for rows.Next() {
		err := rows.Scan(&p.Draws)
		if err != nil {
			rows.Close()
			return nil, fmt.Errorf("failed to fetch player: %s", err)
		}
	}
	rows.Close()

	rows, err = d.db.Query("SELECT COUNT(*) as c FROM "+DatabasePrefix+"match WHERE `game`=? AND `player`=? AND `outcome`=?", game, player, Lost)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch player: %s", err)
	}
	for rows.Next() {
		err := rows.Scan(&p.Losses)
		if err != nil {
			rows.Close()
			return nil, fmt.Errorf("failed to fetch player: %s", err)
		}
	}
	rows.Close()

	return p, nil
}

func (d *Database) Versus(game, player, versus int) (int, int, int, error) {
	d.Lock()
	defer d.Unlock()

	playerMatches := make(map[int]bool)
	versusMatches := make(map[int]bool)

	var matchID int
	var matchPlayer int

	rows, err := d.db.Query("SELECT id, player FROM "+DatabasePrefix+"match WHERE `game`=? AND (`player`=? OR `player`=?)", game, player, versus)
	if err != nil {
		return 0, 0, 0, fmt.Errorf("failed to fetch player: %s", err)
	}
	for rows.Next() {
		err := rows.Scan(&matchID, &matchPlayer)
		if err != nil {
			rows.Close()
			return 0, 0, 0, fmt.Errorf("failed to fetch player: %s", err)
		}

		if matchPlayer == player {
			playerMatches[matchID] = true
		} else {
			versusMatches[matchID] = true
		}
	}
	rows.Close()

	var matches []int
	for matchID := range playerMatches {
		if versusMatches[matchID] {
			matches = append(matches, matchID)
		}
	}

	matches = uniqueInts(matches)

	var wins, draws, losses int
	var addWins, addDraws, addLosses int

	for match := range matches {
		rows, err := d.db.Query("SELECT COUNT(*) as c FROM "+DatabasePrefix+"match WHERE `game`=? AND `match`=? AND `player`=? AND `outcome`=?", game, match, player, Won)
		if err != nil {
			return 0, 0, 0, fmt.Errorf("failed to fetch player: %s", err)
		}
		for rows.Next() {
			err := rows.Scan(&addWins)
			if err != nil {
				rows.Close()
				return 0, 0, 0, fmt.Errorf("failed to fetch player: %s", err)
			}
			wins += addWins
		}
		rows.Close()

		rows, err = d.db.Query("SELECT COUNT(*) as c FROM "+DatabasePrefix+"match WHERE `game`=? AND `player`=? AND `outcome`=?", game, player, Drew)
		if err != nil {
			return 0, 0, 0, fmt.Errorf("failed to fetch player: %s", err)
		}
		for rows.Next() {
			err := rows.Scan(&addDraws)
			if err != nil {
				rows.Close()
				return 0, 0, 0, fmt.Errorf("failed to fetch player: %s", err)
			}
			draws += addDraws
		}
		rows.Close()

		rows, err = d.db.Query("SELECT COUNT(*) as c FROM "+DatabasePrefix+"match WHERE `game`=? AND `player`=? AND `outcome`=?", game, player, Lost)
		if err != nil {
			return 0, 0, 0, fmt.Errorf("failed to fetch player: %s", err)
		}
		for rows.Next() {
			err := rows.Scan(&addLosses)
			if err != nil {
				rows.Close()
				return 0, 0, 0, fmt.Errorf("failed to fetch player: %s", err)
			}
			losses += addLosses
		}
		rows.Close()
	}

	return wins, draws, losses, nil
}

func (d *Database) scanMatch(rows *sql.Rows) (*MatchResult, error) {
	m := &MatchResult{}
	err := rows.Scan(&m.ID, &m.Timestamp, &m.Game, &m.Match, &m.Player, &m.Outcome)
	if err != nil {
		return nil, fmt.Errorf("failed to scan match: %s", err)
	}

	return m, nil
}
