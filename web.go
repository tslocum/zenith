package zenith

import (
	"fmt"
	"html"
	"log"
	"net/http"
)

func newIndexHandler(tracker *Tracker) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// TODO Retrieve top scores

		fmt.Fprintf(w, "HELLO %d, %q", 0, html.EscapeString(r.URL.RequestURI()))
	}
}

func hostWebInterface(tracker *Tracker, address string) {
	handler := http.NewServeMux()

	handler.HandleFunc("/", newIndexHandler(tracker))

	log.Fatal(http.ListenAndServe(address, handler))
}
