package zenith

import (
	"errors"
	"sync"
)

// Tracker records match results and provides player standings.
type Tracker struct {
	db *Database
	sync.RWMutex
}

// TrackerOptions specify options when creating a new Tracker.
type TrackerOptions struct {
	Database *Database
	Web      string // Web interface address
}

// NewTracker returns a new score tracker. A game ID greater than zero must be specified.
func NewTracker(options *TrackerOptions) (*Tracker, error) {
	if options == nil || options.Database == nil {
		return nil, errors.New("failed to initialize tracker: database is a required option")
	}

	t := &Tracker{
		db: options.Database,
	}

	if options.Web != "" {
		go hostWebInterface(t, options.Web)
	}

	return t, nil
}

// Track tracks one or more match results.
func (t *Tracker) Track(results ...*MatchResult) error {
	t.Lock()
	defer t.Unlock()

	err := t.db.Track(results...)
	if err != nil {
		return err
	}

	return nil
}

// PlayersByStanding returns the Players within a specified range of standings.
func (t *Tracker) PlayersByStanding(start int, end int) []*Player {
	return nil
}

// Player returns the Player with the specified ID.
func (t *Tracker) Player(game, player int) (*Player, error) {
	return t.db.Player(game, player)
}
