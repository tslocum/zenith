package zenith

// Player represents the current standing and wins/draws/losses of a player.
type Player struct {
	ID       int
	Standing int
	Wins     int
	Draws    int
	Losses   int
}
