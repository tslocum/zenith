package zenith

// Outcome represents the outcome of a match for a player.
type Outcome int

// Outcomes
const (
	Won  = 1
	Drew = 2
	Lost = 3
)

// MatchResult represents the result of a match for a player. MetaI and MetaS
// may optionally be used to provide additional metadata of the match result.
type MatchResult struct {
	ID        int
	Timestamp int
	Game      int
	Match     int
	Player    int
	Outcome   Outcome

	MetaI map[string]int
	MetaS map[string]string
}
